package com.example.web.mappers;

import com.example.persistence.models.Vehicle;
import com.example.web.dtos.VehicleDto;
import com.google.common.base.Preconditions;

public final class VehicleMapper {

    private VehicleMapper() {
        throw new AssertionError();
    }

    public static VehicleDto convertToDto(final Vehicle vehicle) {
        Preconditions.checkNotNull(vehicle);
        final VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setColor(vehicle.getColor());
        vehicleDto.setRegistrationNo(vehicle.getRegistrationNo());
        vehicleDto.setType(vehicle.getType());
        return vehicleDto;
    }
}
