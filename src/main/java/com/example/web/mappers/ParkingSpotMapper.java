package com.example.web.mappers;

import com.example.persistence.models.ParkingSpot;
import com.example.web.dtos.ParkingSpotDto;
import com.google.common.base.Preconditions;

public final class ParkingSpotMapper {

    private ParkingSpotMapper() {
        throw new AssertionError();
    }

    public static ParkingSpotDto convertToDto(final ParkingSpot parkingSpot) {
        Preconditions.checkNotNull(parkingSpot);
        final ParkingSpotDto parkingSpotDto = new ParkingSpotDto();
        parkingSpotDto.setId(parkingSpot.getId());
        parkingSpotDto.setLevel(parkingSpot.getLevel());
        parkingSpotDto.setOccupied(parkingSpot.getOccupied());
        parkingSpotDto.setRowNumber(parkingSpot.getRowNumber());
        parkingSpotDto.setSpots(parkingSpot.getSpots());
        return parkingSpotDto;
    }

}
