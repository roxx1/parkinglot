package com.example.web.convertors;

import com.example.persistence.models.types.VehicleType;
import org.springframework.core.convert.converter.Converter;

public class StringToVehicleTypeConvertor implements Converter<String, VehicleType> {

    @Override
    public VehicleType convert(final String s) {
        try {
            return VehicleType.valueOf(s);
        } catch (Exception e) {
            return null;
        }
    }
}
