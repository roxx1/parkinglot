package com.example.web.convertors;


import com.example.persistence.models.types.ColorType;
import org.springframework.core.convert.converter.Converter;

public class StringToColorTypeConvertor implements Converter<String, ColorType> {

    @Override
    public ColorType convert(final String s) {
        try{
        return ColorType.valueOf(s);
        }
        catch (Exception e ){
            return null;
        }
    }
}
