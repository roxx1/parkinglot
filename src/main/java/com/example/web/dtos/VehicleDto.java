package com.example.web.dtos;

import com.example.persistence.models.types.ColorType;
import com.example.persistence.models.types.VehicleType;

import java.io.Serializable;

public class VehicleDto implements Serializable {

    private String registrationNo;

    private ColorType color;

    private VehicleType type;

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public ColorType getColor() {
        return color;
    }

    public void setColor(ColorType color) {
        this.color = color;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }
}
