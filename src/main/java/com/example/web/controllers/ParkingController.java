package com.example.web.controllers;

import com.example.services.ParkingService;
import com.example.util.QueryConstants;
import com.example.web.RestPreconditions;
import com.example.web.dtos.ParkingRequestDto;
import com.example.web.dtos.ParkingSpotDto;
import com.example.web.dtos.UnParkingRequestDto;
import com.example.web.dtos.VehicleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/transactions")
public class ParkingController {

    @Autowired
    private ParkingService service;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ParkingSpotDto parkVehicle(@RequestBody final ParkingRequestDto requestDto) {
        RestPreconditions.checkRequestElementNotNull(requestDto);
        return service.parkVehicle(requestDto);
    }

    @PatchMapping("/{ticketNo}")
    @ResponseStatus(HttpStatus.OK)
    public ParkingSpotDto unparkVehicle(@RequestBody final UnParkingRequestDto requestDto, @PathVariable("ticketNo") final String ticketNo) {
        RestPreconditions.checkRequestElementNotNull(requestDto);
        return service.unParkVehicle(requestDto,ticketNo);
    }

    @GetMapping(value = "/vehicles", params = {QueryConstants.START, QueryConstants.END})
    @ResponseStatus(HttpStatus.OK)
    public List<VehicleDto> getVehiclesEnteredInDuration(@RequestParam(QueryConstants.START) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final Date start, @RequestParam(QueryConstants.END) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final Date end) {
        return service.getVehicleEnteredInDuration(start, end);
    }
}
