package com.example.web.controllers;

import com.example.persistence.models.types.ColorType;
import com.example.persistence.models.types.VehicleType;
import com.example.services.VehicleService;
import com.example.util.QueryConstants;
import com.example.web.dtos.VehicleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @GetMapping(params = {QueryConstants.REGISTRATION_NO})
    @ResponseStatus(HttpStatus.OK)
    public List<VehicleDto> getVehiclesWithRegistrationNo(@RequestParam(QueryConstants.REGISTRATION_NO) final String registrationNo) {
        return vehicleService.getListOfVehiclesHavingRegistrationNo(registrationNo);
    }

    @GetMapping(params = {QueryConstants.COLOR})
    @ResponseStatus(HttpStatus.OK)
    public List<VehicleDto> getVehiclesForColor(@RequestParam(QueryConstants.COLOR) final ColorType color) {
        return vehicleService.getListOfVehiclesHavingColor(color);
    }

    @GetMapping(params = {QueryConstants.TYPE})
    @ResponseStatus(HttpStatus.OK)
    public List<VehicleDto> getVehicleBySize(@RequestParam(QueryConstants.TYPE) final VehicleType type) {
        return vehicleService.getListOfVehiclesHavingType(type);
    }
}
