package com.example.services.impl;

import com.example.persistence.ServicePreconditions;
import com.example.persistence.models.Vehicle;
import com.example.persistence.models.types.ColorType;
import com.example.persistence.models.types.VehicleType;
import com.example.persistence.repository.VehicleRepository;
import com.example.services.VehicleService;
import com.example.util.Messages;
import com.example.web.dtos.VehicleDto;
import com.example.web.mappers.VehicleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    private VehicleRepository repository;

    @Override
    public List<VehicleDto> getListOfVehiclesHavingRegistrationNo(final String registrationNo) {
        final List<Vehicle> vehicles = repository.findByRegistrationNo(registrationNo);
        return getVehicleDtos(vehicles);
    }

    @Override
    public List<VehicleDto> getListOfVehiclesHavingColor(final ColorType color) {
        final List<Vehicle> vehicles = repository.findByColor(color);
        return getVehicleDtos(vehicles);
    }

    @Override
    public List<VehicleDto> getListOfVehiclesHavingType(final VehicleType type) {
        final List<Vehicle> vehicles = repository.findByType(type);
        return getVehicleDtos(vehicles);
    }

    public List<VehicleDto> getVehicleDtos(final List<Vehicle> vehicles) {
        ServicePreconditions.checkEntityListExists(vehicles, Messages.VEHICLES_NOTFOUND);
        return vehicles.stream().map(VehicleMapper::convertToDto).collect(Collectors.toList());
    }

}
