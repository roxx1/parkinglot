package com.example.services.impl;

import com.example.persistence.ServicePreconditions;
import com.example.persistence.models.ParkingSpot;
import com.example.persistence.models.ParkingTransaction;
import com.example.persistence.models.Vehicle;
import com.example.persistence.repository.ParkingSpotRepository;
import com.example.persistence.repository.ParkingTransactionRepository;
import com.example.persistence.repository.VehicleRepository;
import com.example.services.ParkingService;
import com.example.util.Messages;
import com.example.web.dtos.ParkingRequestDto;
import com.example.web.dtos.ParkingSpotDto;
import com.example.web.dtos.UnParkingRequestDto;
import com.example.web.dtos.VehicleDto;
import com.example.web.mappers.ParkingSpotMapper;
import com.example.web.mappers.VehicleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ParkingServiceImpl implements ParkingService {

    @Autowired
    private ParkingTransactionRepository transactionRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private ParkingSpotRepository spotRepository;

    @Override
    public ParkingSpotDto parkVehicle(final ParkingRequestDto request) {
        final Vehicle vehicle = getVehicle(request);
        final ParkingSpot spot = getAvailableSpot(request, vehicle);
        ServicePreconditions.checkRequestState(transactionRepository.findByTicketId(request.getTicketNo()) == null, Messages.ALREADY_PARKED);
        spot.setOccupied(Boolean.TRUE);
        final ParkingTransaction transaction = new ParkingTransaction();
        transaction.setTicketId(request.getTicketNo());
        transaction.setVehicle(vehicle);
        transaction.setSpot(spot);
        transactionRepository.save(transaction);
        return ParkingSpotMapper.convertToDto(spot);
    }

    @Override
    public ParkingSpotDto unParkVehicle(final UnParkingRequestDto request, final String ticketNo) {
        final ParkingTransaction transaction = transactionRepository.findByTicketId(ticketNo);
        ServicePreconditions.checkEntityExists(transaction);
        final ParkingSpot spot = transaction.getSpot();
        ServicePreconditions.checkRequestState(spot.getParkingLot().getId() == request.getParkingLotId(), Messages.WRONG_PARKINGLOT_ID);
        ServicePreconditions.checkRequestState(transaction.getUnParkingTime() == null, Messages.NOT_PARKED);
        transaction.setUnParkingTime(new Date());
        transaction.setPrice(request.getPrice());
        spot.setOccupied(Boolean.FALSE);
        transactionRepository.save(transaction);
        return ParkingSpotMapper.convertToDto(spot);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VehicleDto> getVehicleEnteredInDuration(final Date start, final Date end) {
        final List<ParkingTransaction> transactions = transactionRepository.findByParkingTimeIsBetween(start, end);
        ServicePreconditions.checkEntityListExists(transactions);
        return transactions.stream().map(ParkingTransaction::getVehicle).map(VehicleMapper::convertToDto).collect(Collectors.toList());
    }

    public Vehicle getVehicle(final ParkingRequestDto requestDto) {
        final Long vehicleId = requestDto.getVehicleId();
        final Vehicle vehicle = vehicleRepository.findById(vehicleId).orElse(null);
        ServicePreconditions.checkEntityExists(vehicle);
        return vehicle;
    }

    public ParkingSpot getAvailableSpot(final ParkingRequestDto requestDto, final Vehicle vehicle) {
        final List<ParkingSpot> spots = spotRepository.findBySpotsAndParkingLotIdAndOccupied(vehicle.getSpotsNeeded(), requestDto.getParkingLotId(), Boolean.FALSE);
        ServicePreconditions.checkEntityListExists(spots, Messages.SPOT_NOT_EXISTS);
        return spots.get(0);
    }

}
