package com.example.services;

import com.example.web.dtos.ParkingRequestDto;
import com.example.web.dtos.ParkingSpotDto;
import com.example.web.dtos.UnParkingRequestDto;
import com.example.web.dtos.VehicleDto;

import java.util.Date;
import java.util.List;

public interface ParkingService {
    List<VehicleDto> getVehicleEnteredInDuration(final Date start, final Date end);

    ParkingSpotDto parkVehicle(final ParkingRequestDto requestDto);

    ParkingSpotDto unParkVehicle(final UnParkingRequestDto requestDto, final String ticketNo);
}
