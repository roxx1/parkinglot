package com.example.services;

import com.example.persistence.models.types.ColorType;
import com.example.persistence.models.types.VehicleType;
import com.example.web.dtos.VehicleDto;

import java.util.List;

public interface VehicleService {

    List<VehicleDto> getListOfVehiclesHavingRegistrationNo(final String registrationNo);

    List<VehicleDto> getListOfVehiclesHavingColor(final ColorType color);

    List<VehicleDto> getListOfVehiclesHavingType(final VehicleType type);
}
