package com.example.util;

public final class QueryConstants {

    private QueryConstants() {
        throw new AssertionError();
    }

    public static final String COLOR = "color";
    public static final String REGISTRATION_NO = "registration-number";
    public static final String TYPE = "type";
    public static final String START = "start";
    public static final String END = "end";
}
