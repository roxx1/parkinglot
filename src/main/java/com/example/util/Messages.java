package com.example.util;

public final class Messages {

    private Messages() {
        throw new AssertionError();
    }

    public static final String VEHICLES_NOTFOUND = "Vehicles Not Found With Given Id";
    public static final String SPOT_NOT_EXISTS = "No Available Spots Found For Your Vehicle";
    public static final String NOT_PARKED = "Your Vehicle is not yet Parked by given Ticket id or Your Ticket id is Expired";
    public static final String ALREADY_PARKED = "Your Vehicle is already Parked by given Ticket id";
    public static final String WRONG_PARKINGLOT_ID = "Your Parking Lot id is not Correct";
}
