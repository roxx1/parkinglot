package com.example.persistence.setup;

import com.example.persistence.models.*;
import com.example.persistence.models.types.ColorType;
import com.example.persistence.models.types.LevelType;
import com.example.persistence.repository.ParkingLotRepository;
import com.example.persistence.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Component
@Transactional
public class SetupData implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private ParkingLotRepository parkingLotRepository;

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent contextRefreshedEvent) {

        Vehicle v1 = new SmallVehicle();
        v1.setColor(ColorType.White);
        v1.setRegistrationNo("RJ1234");
        vehicleRepository.save(v1);

        Vehicle v2 = new MediumVehicle();
        v2.setColor(ColorType.Yellow);
        v2.setRegistrationNo("BM1234");
        vehicleRepository.save(v2);

        Vehicle v3 = new LargeVehicle();
        v3.setColor(ColorType.Black);
        v3.setRegistrationNo("DL1234");
        vehicleRepository.save(v3);

        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setName("AJ Parking Lot");

        ParkingSpot spot1 = new ParkingSpot();
        spot1.setLevel(LevelType.First);
        spot1.setRowNumber(1);
        spot1.setSpots(1);
        spot1.setParkingLot(parkingLot);


        ParkingSpot spot2 = new ParkingSpot();
        spot2.setLevel(LevelType.Zero);
        spot2.setRowNumber(2);
        spot2.setSpots(3);
        spot2.setParkingLot(parkingLot);


        ParkingSpot spot3 = new ParkingSpot();
        spot3.setLevel(LevelType.Second);
        spot3.setRowNumber(3);
        spot3.setSpots(5);
        spot3.setParkingLot(parkingLot);


        ParkingSpot spot4 = new ParkingSpot();
        spot4.setLevel(LevelType.Zero);
        spot4.setRowNumber(5);
        spot4.setSpots(5);
        spot4.setParkingLot(parkingLot);


        ParkingSpot spot5 = new ParkingSpot();
        spot5.setLevel(LevelType.Zero);
        spot5.setRowNumber(6);
        spot5.setSpots(3);
        spot5.setParkingLot(parkingLot);

        parkingLot.setParkingSpots(Arrays.asList(spot1, spot2, spot3, spot4, spot5));
        parkingLotRepository.save(parkingLot);

    }
}
