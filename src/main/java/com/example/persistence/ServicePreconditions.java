package com.example.persistence;

import com.example.web.exception.MyConflictException;
import com.example.web.exception.MyEntityNotFoundException;

import java.util.List;

public final class ServicePreconditions {

    private ServicePreconditions() {
        throw new AssertionError();
    }

    public static <T> T checkEntityExists(final T entity) {
        return checkEntityExists(entity, null);
    }

    public static <T> T checkEntityExists(final T entity, final String message) {
        if (entity == null) {
            throw new MyEntityNotFoundException(message);
        }
        return entity;
    }

    public static <T> List<T> checkEntityListExists(final List<T> list, final String message) {
        if (list == null || list.isEmpty()) {
            throw new MyEntityNotFoundException(message);
        }
        return list;
    }

    public static <T> List<T> checkEntityListExists(final List<T> list) {
        return checkEntityListExists(list, null);
    }

    public static void checkRequestState(final boolean expression) {
        checkRequestState(expression, null);
    }

    public static void checkRequestState(final boolean expression, final String message) {
        if (!expression) {
            throw new MyConflictException(message);
        }
    }
}