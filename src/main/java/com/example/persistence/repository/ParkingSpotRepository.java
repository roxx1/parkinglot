package com.example.persistence.repository;

import com.example.persistence.models.ParkingSpot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParkingSpotRepository extends JpaRepository<ParkingSpot, Long> {
    List<ParkingSpot> findBySpotsAndParkingLotIdAndOccupied(final Integer spots, final Long parkingLotId, final Boolean occupied);
}
