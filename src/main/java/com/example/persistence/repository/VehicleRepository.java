package com.example.persistence.repository;

import com.example.persistence.models.Vehicle;
import com.example.persistence.models.types.ColorType;
import com.example.persistence.models.types.VehicleType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    List<Vehicle> findByRegistrationNo(final String registrationNo);

    List<Vehicle> findByColor(final ColorType colorType);

    List<Vehicle> findByType(final VehicleType type);
}
