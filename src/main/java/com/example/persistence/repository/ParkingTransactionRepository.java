package com.example.persistence.repository;

import com.example.persistence.models.ParkingTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ParkingTransactionRepository extends JpaRepository<ParkingTransaction, Long> {

    List<ParkingTransaction> findByParkingTimeIsBetween(final Date start, final Date end);

    ParkingTransaction findByTicketId(final String ticketId);
}
