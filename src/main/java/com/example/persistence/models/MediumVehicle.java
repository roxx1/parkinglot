package com.example.persistence.models;

import com.example.persistence.models.types.VehicleType;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "medium_vehicle")
public class MediumVehicle extends Vehicle {

    public MediumVehicle() {
        super(3, VehicleType.Medium);
    }
}
