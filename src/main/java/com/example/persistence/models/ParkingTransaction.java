package com.example.persistence.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "transaction")
public class ParkingTransaction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "parking_time")
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date parkingTime;

    @Column(name = "unparking_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date unParkingTime;

    @Column(name = "price")
    private Double price;

    @Column(name = "ticketId", unique = true, nullable = false)
    private String ticketId;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "spot_id")
    private ParkingSpot spot;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getParkingTime() {
        return parkingTime;
    }

    public void setParkingTime(Date parkingTime) {
        this.parkingTime = parkingTime;
    }

    public Date getUnParkingTime() {
        return unParkingTime;
    }

    public void setUnParkingTime(Date unParkingTime) {
        this.unParkingTime = unParkingTime;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public ParkingSpot getSpot() {
        return spot;
    }

    public void setSpot(ParkingSpot spot) {
        this.spot = spot;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
