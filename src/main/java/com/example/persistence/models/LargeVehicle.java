package com.example.persistence.models;


import com.example.persistence.models.types.VehicleType;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "large_vehicle")
public class LargeVehicle extends Vehicle {

    public LargeVehicle() {
        super(5, VehicleType.Large);
    }

}
