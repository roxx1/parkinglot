package com.example.persistence.models.types;

public enum LevelType {
    Zero, First, Second
}
