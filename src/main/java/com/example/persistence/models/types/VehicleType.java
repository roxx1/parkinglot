package com.example.persistence.models.types;

public enum VehicleType {
    Small, Medium, Large
}
