package com.example.persistence.models.types;

public enum ColorType {
    Red, Green, White, Black, Yellow, Grey, Orange, Purple;
}
