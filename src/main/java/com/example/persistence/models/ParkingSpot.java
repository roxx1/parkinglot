package com.example.persistence.models;

import com.example.persistence.models.types.LevelType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "spot")
public class ParkingSpot implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "row_number")
    private Integer rowNumber;

    @Column(name = "spots_count")
    private Integer spots;

    @Column(name = "occupied")
    private Boolean occupied;

    @Enumerated(EnumType.STRING)
    @Column(name = "level")
    private LevelType level;

    @OneToMany(mappedBy = "spot", cascade = CascadeType.ALL)
    private List<ParkingTransaction> transactions;

    @ManyToOne
    @JoinColumn(name = "parking_lot_id")
    private ParkingLot parkingLot;

    public ParkingSpot() {
        occupied = Boolean.FALSE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getSpots() {
        return spots;
    }

    public void setSpots(Integer spots) {
        this.spots = spots;
    }

    public Boolean getOccupied() {
        return occupied;
    }

    public void setOccupied(Boolean occupied) {
        this.occupied = occupied;
    }

    public LevelType getLevel() {
        return level;
    }

    public void setLevel(LevelType level) {
        this.level = level;
    }

    public List<ParkingTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<ParkingTransaction> transactions) {
        this.transactions = transactions;
    }

    public ParkingLot getParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }
}
