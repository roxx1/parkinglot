package com.example.persistence.models;

import com.example.persistence.models.types.ColorType;
import com.example.persistence.models.types.VehicleType;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "vehicle")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Vehicle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "registration_number", unique = true, nullable = false)
    private String registrationNo;

    @Enumerated(EnumType.STRING)
    @Column(name = "color")
    private ColorType color;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private VehicleType type;

    @Column(name = "spots_needed", nullable = false)
    private Integer spotsNeeded;

    public Vehicle() {
    }

    public Vehicle(Integer spotsNeeded, VehicleType type) {
        this.spotsNeeded = spotsNeeded;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public ColorType getColor() {
        return color;
    }

    public void setColor(ColorType color) {
        this.color = color;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public Integer getSpotsNeeded() {
        return spotsNeeded;
    }

    public void setSpotsNeeded(Integer spotsNeeded) {
        this.spotsNeeded = spotsNeeded;
    }
}
