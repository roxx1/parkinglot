package com.example.persistence.models;

import com.example.persistence.models.types.VehicleType;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "small_vehicle")
public class SmallVehicle extends Vehicle {

    public SmallVehicle() {
        super(1, VehicleType.Small);
    }

}
