package com.example.config;

import com.example.web.convertors.StringToColorTypeConvertor;
import com.example.web.convertors.StringToVehicleTypeConvertor;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToColorTypeConvertor());
        registry.addConverter(new StringToVehicleTypeConvertor());
    }
}
